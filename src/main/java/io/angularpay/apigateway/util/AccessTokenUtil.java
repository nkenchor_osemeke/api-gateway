package io.angularpay.apigateway.util;

import io.angularpay.apigateway.configurations.AngularPayConfiguration;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

@Service
@Slf4j
public class AccessTokenUtil {

    private final RSAPublicKey publicKey;

    public AccessTokenUtil(AngularPayConfiguration angularPayConfiguration) {
        try {
            byte[] publicKeyBytes = Base64.getDecoder().decode(angularPayConfiguration.getSecurity().getPublicKey());
            publicKey = getPublicKey(publicKeyBytes);
        } catch (Exception exception) {
            throw new RuntimeException("Unable to parse public key", exception);
        }
    }

    private RSAPublicKey getPublicKey(byte[] publicKeyBytes) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(publicKeyBytes);
        return (RSAPublicKey) keyFactory.generatePublic(keySpecX509);
    }

    private PrivateKey getPrivateKey(byte[] privateKeyBytes) throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(privateKeyBytes);
        return keyFactory.generatePrivate(keySpecPKCS8);
    }

    public boolean verifyToken(String accessToken) {
        try {
            Jwts.parser().setSigningKey(this.publicKey).parseClaimsJws(accessToken).getBody();
            return true;
        } catch (SignatureException | MalformedJwtException | ExpiredJwtException |
                UnsupportedJwtException | IllegalArgumentException exception) {
            return false;
        }
    }

    public Optional<Claims> parseClaims(String accessToken) {
        try {
            return Optional.of(Jwts.parser().setSigningKey(this.publicKey).parseClaimsJws(accessToken).getBody());
        } catch (SignatureException | MalformedJwtException | ExpiredJwtException |
                UnsupportedJwtException | IllegalArgumentException exception) {
            return Optional.empty();
        }
    }
}



