package io.angularpay.apigateway.common;

public class Constants {
    public static final String REVOKED_TOKEN_HASH = "identity-revoked-tokens";
}
