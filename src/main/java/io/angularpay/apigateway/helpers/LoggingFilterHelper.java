package io.angularpay.apigateway.helpers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.web.server.ServerWebExchange;

import static io.angularpay.apigateway.helpers.Helper.addToMappedDiagnosticContext;

@Slf4j
public class LoggingFilterHelper {

    public static void logRequest(ServerWebExchange serverWebExchange) {
        HttpHeaders headers = serverWebExchange.getRequest().getHeaders();

        String USERNAME = "x-angularpay-username";
        String USER_REFERENCE = "x-angularpay-user-reference";
        String USER_ROLES = "x-angularpay-user-role";
        String REQUEST_PATH = "path";
        String REQUEST_METHOD = "method";
        String QUERY_PARAMETERS = "query_parameters";
        String HOST = "Host";
        String APP_HOST = "x-angularpay-application-host";
        String REAL_IP = "X-Real-IP";
        String FORWARDED_FOR = "X-Forwarded-For";
        String FORWARDED_HOST = "X-Forwarded-Host";
        String USER_AGENT = "User-Agent";

        addToMappedDiagnosticContext(USERNAME, headers.get(USERNAME));
        addToMappedDiagnosticContext(USER_REFERENCE, headers.get(USER_REFERENCE));
        addToMappedDiagnosticContext(USER_ROLES, headers.get(USER_ROLES));
        addToMappedDiagnosticContext(REQUEST_PATH, serverWebExchange.getRequest().getURI());
        addToMappedDiagnosticContext(REQUEST_METHOD, serverWebExchange.getRequest().getMethod());
        addToMappedDiagnosticContext(QUERY_PARAMETERS, serverWebExchange.getRequest().getQueryParams());
        addToMappedDiagnosticContext(APP_HOST, headers.get(HOST));
        addToMappedDiagnosticContext(REAL_IP, headers.get(REAL_IP));
        addToMappedDiagnosticContext(FORWARDED_FOR, headers.get(FORWARDED_FOR));
        addToMappedDiagnosticContext(FORWARDED_HOST, headers.get(FORWARDED_HOST));
        addToMappedDiagnosticContext(USER_AGENT, headers.get(USER_AGENT));

        log.info("Request received on API Gateway");
    }
}
