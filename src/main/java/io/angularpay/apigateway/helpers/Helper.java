package io.angularpay.apigateway.helpers;

import io.angularpay.apigateway.models.LiveFeedAuthModel;
import io.angularpay.apigateway.models.UserNotificationAuthModel;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
public class Helper {

    public static void addToMappedDiagnosticContext(String name, Object header) {
        if (Objects.isNull(header)) return;
        String value;
        if (header instanceof List) {
            value = String.join(",", (List) header);
        } else {
            value = header.toString();
        }
        if (StringUtils.hasText(value)) {
            MDC.put(name, value);
        }
    }

    public static void addToMappedDiagnosticContextOrRandomUUID(String name, Object header) {
        if (Objects.nonNull(header)) {
            String value;
            if (header instanceof List && !CollectionUtils.isEmpty((List) header)) {
                value = String.valueOf(((List) header).get(0));
            } else {
                value = header.toString();
            }
            MDC.put(name, value);
        } else {
            MDC.put(name, UUID.randomUUID().toString());
        }
    }

    public static LiveFeedAuthModel toLiveFeedAuthModel(MultiValueMap<String, String> queryParams) {
        boolean valid = !CollectionUtils.isEmpty(queryParams)
                && StringUtils.hasText(queryParams.getFirst("device_id"))
                && StringUtils.hasText(queryParams.getFirst("access_token"));

        if (valid) {
            return LiveFeedAuthModel.builder()
                    .valid(true)
                    .deviceId(queryParams.getFirst("device_id"))
                    .accessToken(queryParams.getFirst("access_token"))
                    .build();
        } else {
            return LiveFeedAuthModel.builder()
                    .valid(false)
                    .build();
        }
    }

    public static UserNotificationAuthModel toUserNotificationAuthModel(MultiValueMap<String, String> queryParams) {
        boolean valid = !CollectionUtils.isEmpty(queryParams)
                && StringUtils.hasText(queryParams.getFirst("device_id"))
                && StringUtils.hasText(queryParams.getFirst("user_reference"))
                && StringUtils.hasText(queryParams.getFirst("notification_preferences"))
                && StringUtils.hasText(queryParams.getFirst("access_token"));

        if (valid) {
            return UserNotificationAuthModel.builder()
                    .valid(true)
                    .deviceId(queryParams.getFirst("device_id"))
                    .userReference(queryParams.getFirst("user_reference"))
                    .notificationPreferences(queryParams.getFirst("notification_preferences"))
                    .accessToken(queryParams.getFirst("access_token"))
                    .build();
        } else {
            return UserNotificationAuthModel.builder()
                    .valid(false)
                    .build();
        }
    }

}
