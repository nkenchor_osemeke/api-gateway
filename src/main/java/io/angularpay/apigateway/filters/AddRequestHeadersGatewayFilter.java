package io.angularpay.apigateway.filters;

import io.angularpay.apigateway.models.AuthCredentials;
import io.angularpay.apigateway.models.TokenClaims;
import io.angularpay.apigateway.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.cors.reactive.CorsUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

import static io.angularpay.apigateway.helpers.Helper.addToMappedDiagnosticContextOrRandomUUID;
import static io.angularpay.apigateway.helpers.LoggingFilterHelper.logRequest;

@Slf4j
@Component
@RequiredArgsConstructor
public class AddRequestHeadersGatewayFilter implements GlobalFilter, Ordered {

    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        if (CorsUtils.isCorsRequest(serverHttpRequest)) {
            ServerHttpResponse serverHttpResponse = exchange.getResponse();
            HttpHeaders headers = serverHttpResponse.getHeaders();
            headers.set("Access-Control-Max-Age", "7200");
            if (serverHttpRequest.getMethod() == HttpMethod.OPTIONS) {
                serverHttpResponse.setStatusCode(HttpStatus.OK);
                return Mono.empty();
            }
        }

        String CORRELATION_ID = "x-angularpay-correlation-id";
        HttpHeaders headers = exchange.getRequest().getHeaders();
        addToMappedDiagnosticContextOrRandomUUID(CORRELATION_ID, headers.get(CORRELATION_ID));

        try {
            Optional<AuthCredentials> authCredentials = jwtTokenProvider.resolveCredentials(exchange);
            if (authCredentials.isEmpty()) {
                ServerHttpRequest request = exchange.getRequest()
                        .mutate()
                        .header(CORRELATION_ID, MDC.get(CORRELATION_ID))
                        .build();
                ServerWebExchange mutated = exchange.mutate().request(request).build();
                logRequest(exchange);
                return chain.filter(mutated);
            }

            Optional<TokenClaims> optionalTokenClaims = jwtTokenProvider.parseClaims(authCredentials.get().getToken());
            if (optionalTokenClaims.isEmpty()) {
                ServerHttpRequest request = exchange.getRequest()
                        .mutate()
                        .header(CORRELATION_ID, MDC.get(CORRELATION_ID))
                        .build();
                ServerWebExchange mutated = exchange.mutate().request(request).build();
                logRequest(exchange);
                return chain.filter(mutated);
            }

            TokenClaims claims = optionalTokenClaims.get();
            ServerHttpRequest request = exchange.getRequest()
                    .mutate()
                    .header(CORRELATION_ID, MDC.get(CORRELATION_ID))
                    .header("x-angularpay-username", claims.getUserName())
                    .header("x-angularpay-user-reference", claims.getUserReference())
                    .header("x-angularpay-user-roles", commaSeparated(claims.getAuthorities()))
                    .build();
            logRequest(exchange);
            ServerWebExchange mutated = exchange.mutate().request(request).build();
            return chain.filter(mutated);
        } finally {
            MDC.clear();
        }
    }

    private String commaSeparated(List<String> authorities) {
        if(CollectionUtils.isEmpty(authorities)) return "";
        return String.join(",", authorities);
    }

    @Override
    public int getOrder() {
        return -1;
    }


}