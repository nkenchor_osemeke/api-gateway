package io.angularpay.apigateway.ports.outbound;

import java.util.Map;

public interface OutboundMessagingPort {
    boolean isTokenRevoked(String reference);
    void removeIfExpired(String reference);
    Map<String, String> getPlatformConfigurations(String hashName);
}
