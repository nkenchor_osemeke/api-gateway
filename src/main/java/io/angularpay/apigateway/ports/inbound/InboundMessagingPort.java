package io.angularpay.apigateway.ports.inbound;

import io.angularpay.apigateway.models.platform.PlatformConfigurationIdentifier;

public interface InboundMessagingPort {
    void onMessage(String message, PlatformConfigurationIdentifier topic);
}
