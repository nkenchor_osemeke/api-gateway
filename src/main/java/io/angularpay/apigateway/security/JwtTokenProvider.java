package io.angularpay.apigateway.security;

import io.angularpay.apigateway.models.AuthCredentials;
import io.angularpay.apigateway.models.TokenClaims;
import io.angularpay.apigateway.models.TokenType;
import io.angularpay.apigateway.util.AccessTokenUtil;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;

import java.util.*;
import java.util.stream.Collectors;

import static io.angularpay.apigateway.models.TokenType.ACCESS_TOKEN;
import static io.angularpay.apigateway.models.TokenType.REFRESH_TOKEN;

@Service
@Slf4j
@RequiredArgsConstructor
public class JwtTokenProvider {

    private final AccessTokenUtil accessTokenUtil;

    public Optional<TokenClaims> parseClaims(String token) {
        Optional<Claims> optionalClaims = this.accessTokenUtil.parseClaims(token);
        if (optionalClaims.isEmpty()) {
            return Optional.empty();
        }

        Claims claims = optionalClaims.get();

        TokenType type = "Refresh".equalsIgnoreCase(claims.get("typ").toString()) ? REFRESH_TOKEN : ACCESS_TOKEN;

        TokenClaims tokenClaims = new TokenClaims();
        tokenClaims.setDeviceId(claims.get("azp").toString());
        tokenClaims.setJti(claims.getId());
        tokenClaims.setUserName(claims.getSubject());
        tokenClaims.setUserReference(claims.get("user_reference").toString());
        tokenClaims.setAuthorities(claims.get("roles", ArrayList.class));
        tokenClaims.setType(type);
        return Optional.of(tokenClaims);
    }

    public Optional<AuthCredentials> resolveCredentials(ServerWebExchange exchange) {
        HttpHeaders headers = exchange.getRequest().getHeaders();

        String deviceId = headers.getFirst("x-angularpay-device-id");
        if (!StringUtils.hasText(deviceId)) {
            return Optional.empty();
        }

        String authHeader = headers.getFirst(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.hasText(authHeader)) {
            return Optional.empty();
        }

        if (!authHeader.startsWith("Bearer ")) {
            return Optional.empty();
        }

        return Optional.of(AuthCredentials.builder()
                .token(authHeader.substring(7))
                .deviceId(deviceId)
                .build());
    }

    public List<GrantedAuthority> getAuthority(String token) {
        return parseClaims(token).<List<GrantedAuthority>>map(tokenClaims -> tokenClaims.getAuthorities().stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

}