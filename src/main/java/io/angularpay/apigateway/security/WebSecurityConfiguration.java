package io.angularpay.apigateway.security;

import io.angularpay.apigateway.configurations.AngularPayConfiguration;
import io.angularpay.apigateway.models.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

import static io.angularpay.apigateway.models.Role.*;

@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@RequiredArgsConstructor
public class WebSecurityConfiguration {

    private final SecurityContextRepository securityContextRepository;
    private final AngularPayConfiguration configuration;

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        ServerHttpSecurity.AuthorizeExchangeSpec builder = http
                .exceptionHandling()
                .authenticationEntryPoint((swe, e) -> Mono.fromRunnable(() -> {
                    swe.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
                })).accessDeniedHandler((swe, e) -> Mono.fromRunnable(() -> {
                    swe.getResponse().setStatusCode(HttpStatus.FORBIDDEN);
                }))
                .and()
                .csrf().disable()
                .formLogin().disable()
                .httpBasic().disable()
                .authenticationManager(securityContextRepository.getAuthenticationManager())
                .securityContextRepository(securityContextRepository)
                .authorizeExchange()
                .pathMatchers(HttpMethod.OPTIONS).permitAll()
                .pathMatchers(HttpMethod.GET,
                        "/onboarding/signups/statistics").hasAnyAuthority(
                        ROLE_PLATFORM_ADMIN.name(),
                        ROLE_PLATFORM_USER.name()
                )
                .pathMatchers("/onboarding/**").hasAuthority(ROLE_ONBOARDING_USER.name())
                .pathMatchers(HttpMethod.POST,
                        "/identity/auth/login",
                        "/identity/auth/logout"
                ).permitAll()
                .pathMatchers(HttpMethod.POST,
                        "/identity/users/forgot-password/start").hasAuthority(ROLE_FORGOT_PASSWORD_USER.name())
                .pathMatchers(HttpMethod.PUT,
                        "/identity/users/{userReference}/forgot-password/**").hasAuthority(ROLE_FORGOT_PASSWORD_USER.name())
                .pathMatchers(HttpMethod.POST, "/subscriptions").permitAll()
                .pathMatchers(HttpMethod.POST, "/inquiries/partners").permitAll()
                .pathMatchers(HttpMethod.GET, "/actuator/health").permitAll()
                .pathMatchers("/corporate/**").permitAll()
                .pathMatchers("/admin/**").permitAll()
                .pathMatchers("/otp/**").denyAll()
                .pathMatchers("/notification/**").denyAll()
                .pathMatchers("/maturity/**").denyAll()
                .pathMatchers("/cipher/**").denyAll()
                .pathMatchers("/transaction/**").denyAll();

        if (configuration.isWebsocketsTestEnabled()) {
            // TODO: for WS test only! Disable when NOT in use
            return builder
                    .pathMatchers("/test-un").permitAll()
                    .pathMatchers("/test-un/webjars/**").permitAll()
                    .pathMatchers("/test-un/main.css").permitAll()
                    .pathMatchers("/test-un/app.js").permitAll()
                    .pathMatchers("/test-lf").permitAll()
                    .pathMatchers("/test-lf/webjars/**").permitAll()
                    .pathMatchers("/test-lf/main.css").permitAll()
                    .pathMatchers("/test-lf/app.js").permitAll()
                    .anyExchange().hasAnyAuthority(Role.knownUserRoles())
                    .and().build();
        } else {
            return builder.anyExchange().hasAnyAuthority(Role.knownUserRoles())
                    .and().build();
        }
    }
}