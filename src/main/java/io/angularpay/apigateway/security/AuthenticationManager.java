package io.angularpay.apigateway.security;

import io.angularpay.apigateway.adapters.outbound.RedisAdapter;
import io.angularpay.apigateway.models.AuthCredentials;
import io.angularpay.apigateway.models.LiveFeedAuthModel;
import io.angularpay.apigateway.models.TokenClaims;
import io.angularpay.apigateway.models.UserNotificationAuthModel;
import io.angularpay.apigateway.util.AccessTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static io.angularpay.apigateway.helpers.Helper.toLiveFeedAuthModel;
import static io.angularpay.apigateway.helpers.Helper.toUserNotificationAuthModel;
import static io.angularpay.apigateway.helpers.LoggingFilterHelper.logRequest;
import static io.angularpay.apigateway.models.TokenType.REFRESH_TOKEN;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthenticationManager implements ReactiveAuthenticationManager {

    private final JwtTokenProvider tokenProvider;
    private final RedisAdapter messagingPort;
    private final AccessTokenUtil accessTokenUtil;

    @Override
    public Mono<Authentication> authenticate(Authentication authentication) {
        String authToken = authentication.getCredentials().toString();

        if (!StringUtils.hasText(authToken)) {
            log.info("Access token not set in authentication credentials");
            return Mono.empty();
        }

        log.info("processing access token: {}", authToken);

        if (!this.accessTokenUtil.verifyToken(authToken)) {
            log.info("Token verification failed. Invalid access token: {}", authToken);
            return Mono.empty();
        }

        Optional<TokenClaims> optionalTokenClaims = tokenProvider.parseClaims(authToken);

        if (optionalTokenClaims.isEmpty()) {
            log.info("no claims found in access token: {}", authToken);
            return Mono.empty();
        }

        TokenClaims claims = optionalTokenClaims.get();
        String username = claims.getUserName();
        String jti = claims.getJti();

        if (this.messagingPort.isTokenRevoked(jti)) {
            log.info("access token is revoked: {}", authToken);
            this.messagingPort.removeIfExpired(jti);
            return Mono.empty();
        }

        if (!authentication.getPrincipal().toString().equalsIgnoreCase(claims.getDeviceId())) {
            log.info("access token NOT sent from user device: {}", authToken);
            return Mono.empty();
        }

        log.info("valid token, user {} is logged-in", username);
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                username, null, tokenProvider.getAuthority(authToken));
        return Mono.just(auth);
    }

    public Mono<Authentication> authenticate(ServerWebExchange exchange) {
        logRequest(exchange);

        String requestPath = exchange.getRequest().getPath().value();
        String accessToken = "";
        Optional<AuthCredentials> optionalAuthCredentials = Optional.empty();

        if (requestPath.startsWith("/livefeed/connect") || requestPath.startsWith("/user-notifications/connect")) {
            accessToken = exchange.getRequest().getQueryParams().getFirst("access_token");
        } else {
            optionalAuthCredentials = tokenProvider.resolveCredentials(exchange);
            if (optionalAuthCredentials.isPresent()) {
                accessToken = optionalAuthCredentials.get().getToken();
            }
        }

        if (StringUtils.hasText(accessToken)) {
            Optional<TokenClaims> optional = tokenProvider.parseClaims(accessToken);
            if (!requestPath.equalsIgnoreCase("/identity/auth/token/refresh")
                    && optional.isPresent() && optional.get().getType() == REFRESH_TOKEN) {
                log.info("Cannot use refresh token to access protected endpoints");
                return Mono.empty();
            }
        }

        if (requestPath.startsWith("/livefeed/connect")) {
            log.info("Authenticating Livefeed connection");
            LiveFeedAuthModel liveFeedAuthModel = toLiveFeedAuthModel(exchange.getRequest().getQueryParams());
            if (liveFeedAuthModel.isValid()) {
                return authenticate(new UsernamePasswordAuthenticationToken(
                        liveFeedAuthModel.getDeviceId(),
                        liveFeedAuthModel.getAccessToken())
                );
            } else {
                log.info("No valid credentials provided in livefeed connection request");
                return Mono.empty();
            }
        }

        if (requestPath.startsWith("/user-notifications/connect")) {
            log.info("Authenticating User Notifications connection");
            UserNotificationAuthModel userNotificationAuthModel = toUserNotificationAuthModel(exchange.getRequest().getQueryParams());
            if (userNotificationAuthModel.isValid()) {
                return authenticate(new UsernamePasswordAuthenticationToken(
                        userNotificationAuthModel.getDeviceId(),
                        userNotificationAuthModel.getAccessToken())
                );
            } else {
                log.info("No valid credentials provided in user notification connection request");
                return Mono.empty();
            }
        }

        if (optionalAuthCredentials.isEmpty()) {
            log.info("No valid credentials provided for protected endpoint request");
            return Mono.empty();
        }

        return authenticate(new UsernamePasswordAuthenticationToken(
                optionalAuthCredentials.get().getDeviceId(),
                optionalAuthCredentials.get().getToken()
        ));
    }
}
