package io.angularpay.apigateway.adapters.inbound;

import io.angularpay.apigateway.domain.commands.PlatformConfigurationsConverterCommand;
import io.angularpay.apigateway.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.apigateway.ports.inbound.InboundMessagingPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static io.angularpay.apigateway.models.platform.PlatformConfigurationSource.TOPIC;

@Service
@RequiredArgsConstructor
public class RedisMessageAdapter implements InboundMessagingPort {

    private final PlatformConfigurationsConverterCommand converterCommand;

    @Override
    public void onMessage(String message, PlatformConfigurationIdentifier identifier) {
        this.converterCommand.execute(message, identifier, TOPIC);
    }
}
