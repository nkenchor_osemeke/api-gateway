package io.angularpay.apigateway.models;

public enum TokenType {
    ACCESS_TOKEN, REFRESH_TOKEN
}
