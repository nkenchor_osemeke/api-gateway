package io.angularpay.apigateway.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthCredentials {
    private String token;
    private String deviceId;
}
