package io.angularpay.apigateway.models;

import lombok.Data;

import java.util.List;

@Data
public class TokenClaims {
    private String deviceId;
    private String userReference;
    private String userName;
    private String jti;
    private List<String> authorities;
    private TokenType type;
}
