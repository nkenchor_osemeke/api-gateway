package io.angularpay.apigateway.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserNotificationAuthModel {
    private String deviceId;
    private String userReference;
    private String notificationPreferences;
    private String accessToken;
    private boolean valid;
}
