package io.angularpay.apigateway.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LiveFeedAuthModel {
    private String deviceId;
    private String accessToken;
    private boolean valid;
}
