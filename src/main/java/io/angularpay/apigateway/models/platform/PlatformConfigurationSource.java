
package io.angularpay.apigateway.models.platform;

import lombok.Getter;

@Getter
public enum PlatformConfigurationSource {
    HASH, TOPIC
}
